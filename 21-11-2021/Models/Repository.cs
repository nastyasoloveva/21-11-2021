using System.Collections.Generic;

namespace _21_11_2021.Models
{
    public static class Repository
    {
        private static List<StudentResponse> responses = new List<StudentResponse>();

        public static IEnumerable<StudentResponse> Responses => responses;

        public static void AddResponse(StudentResponse response)
        {
            responses.Add(response);
        }

    }
}
