public class StudentResponse
{
    [Required(ErrorMessage = "Please enter your name")]
    public string Name { get; set; }

    [Required(ErrorMessage = "Please enter your email address")]
    [EmailAddress]
    public string Email { get; set; }
    
    [Required(ErrorMessage = "Please enter your university")]
    public string University { get; set; }

    [Required(ErrorMessage = "Please choose course")]
    public enum Course {

        [Display(Name = "1")]
        First,
        [Display(Name = "2")]
        Second,
        [Display(Name = "3")]
        Third,
        [Display(Name = "4")]
        Fuorth
    }
    public Course Number { get; set; }

}
